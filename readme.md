# CIGeoE Identify Dangles

This plugin toggle label visibility.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “feature_z_setter” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “Feature Z Setter”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “feature_z_setter” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - Click on plugin icon to open a widget containing 1 combo box, 1 text box and 3 buttons.

![ALT](/images/image5.png)
- In the combo box, one can select the DEM layer (from the open layers in the "Layers panel").
- In the text box, one can define the z-axis offset value related to the DEM z value ( or zero in case there is no DEM defined or when one is editing features outside the selected DEM area).
- The refresh button updates the layers identified in the combo box.
- The flatten features button puts the z-axis values of the vertices in the selected feature with the highest z-axis value of them (this operation can use one or more features simultaneously).
- The Drape button puts the z-axis value of all vertices of the selected feature with the DEM value plus the offset value.

## Example

We opened a DEM and two polygon shapes: ```A_Arvoredo_esparso``` and ```A_Arvoredo_denso```. Beside that, we defined in the combo box the layer with the DEM and an offset of 10 meters.

![ALT](/images/image6.png)

When creating a new polygon in ```A_Arvoredo_esparso```, the vertices assume the z-axis value corresponding to the DEM z-axis value plus the offset value:

![ALT](/images/image7.png)

To level the recently created feature to the highest z-axis value, select the feature and click the ```flatten feature(s)``` button.

![ALT](/images/image8.png)

To rollback the z-axis value to the DEM z-axis value, select the feature and click on the ```drape feature(s)``` button. In the example, the offset value changed to zero. You can now compare the z-axis values of the following image with the previous ones.

![ALT](/images/image9.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
