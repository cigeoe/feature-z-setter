# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Feature Z Setter
qgisMinimumVersion=3.0
description=Sets the Z value of each new/edited feature to a specific value, based on a DEM layer plus an offset. 
version=1.0
author=Centro de Informação Geoespacial do Exército 
email=igeoe@igeoe.pt

about=This plugin's aim is to help to set, more accurately, the 3D coordinate of vector features. It does it by searching the Z value of a preloaded raster DEM, with the chance of fine tunning that value by setting an offset. If no DEM is provided, it will only consider the offset value.

tracker=https://gitlab.com/cigeoe/feature-z-setter/-/issues
repository=https://gitlab.com/cigeoe/feature-z-setter
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=3d,raster,vector,dem,layers

homepage=https://www.igeoe.pt
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

